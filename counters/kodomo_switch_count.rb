require_relative '../common'
require 'selenium-webdriver'

ua = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36"
caps = Selenium::WebDriver::Remote::Capabilities.chrome("chromeOptions" => {args: ["--headless","--no-sandbox", "--disable-setuid-sandbox", "--disable-gpu", "--user-agent=#{ua}", 'window-size=1280x800']})
 # caps = Selenium::WebDriver::Remote::Capabilities.chrome("chromeOptions" => {args: ["--user-agent=#{ua}", "window-size=1280x800"]})
$session = Selenium::WebDriver.for :chrome, desired_capabilities: caps

def query_click(css_selector)
  javascript_statement = %Q{return document.querySelector("#{css_selector}")}
  res = $session.execute_script(javascript_statement)
  return if res.nil?
  javascript_statement = %Q{document.querySelector("#{css_selector}").click()}
  $session.execute_script(javascript_statement)
  sleep 1
end

def send_value(css_selector,value)
  javascript_statement = %Q{document.querySelector("#{css_selector}").value = "#{value}"}
  $session.execute_script(javascript_statement)
end

def count_result
  $session.find_element(:css,'h2.SearchView-title').text.gsub(/\D/,'').to_i
end

$session.navigate.to 'https://kodomo-switch.com/?search_element_0=&search_element_1=&search_element_2_cnt=13&search_element_3_cnt=8&search_element_4_cnt=24&search_element_5_cnt=6&searchbutton=%E6%A4%9C%E7%B4%A2%E3%81%99%E3%82%8B&csp=search_add&feadvns_max_line_14=6&fe_form_no=14'
sleep 1
doc = Nokogiri::HTML.parse($session.page_source, nil, 'utf-8')
pref_id_hash = {}
doc.css('select[name=search_element_0]>option').each do |option_ele|
  pref_id_hash[option_ele.text] = option_ele[:value]
end

$lesson_counter = 0
  pref_id_hash.each do |pref_name, pref_id|
  puts '##' + pref_name
  send_value('select[name=search_element_0]',pref_id)
  query_click('#feas-submit-button-18')
  puts count_result
  $lesson_counter += count_result
end
puts 'lesson_sum_count ='
puts $lesson_counter
