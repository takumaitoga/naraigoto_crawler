require_relative '../common'

BASE_URL = "https://skuroo.net/list"
def next_page_doc(doc)
  next_ele = doc.at_css('li.next')
  return if next_ele.classes.include?('disabled')
  next_url = shape_url(next_ele.at_css('>a')[:href],BASE_URL)
  url = url_to_doc(next_url)
end

def count_lesson_per_pref(url)
  doc = url_to_doc(url)
  loop do
    $lesson_counter += doc.css("div.item").length
    doc = next_page_doc(doc)
    break if doc.nil?
  end
end

$lesson_counter = 0
doc = url_to_doc(BASE_URL)
# 都道府県別のurlを作成
pref_url_hash = {}
doc.css('#search-prefecture>option').each do |option_ele|
  pref_url_hash[option_ele.text] = "#{BASE_URL}?pref=#{option_ele[:value]}"
end
puts '■都道府県数'
puts pref_url_hash.length
pref_url_hash.each do |pref_name, pref_url|
  before_counter = $lesson_counter
  puts '##' + pref_name
  count_lesson_per_pref(pref_url)
  puts $lesson_counter - before_counter
end
puts 'lesson_sum_count='
puts $lesson_counter
