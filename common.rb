require 'open-uri'
require 'nokogiri'

def url_to_doc(url)
  sleep 1
  html = open(url).read
  doc = Nokogiri::HTML.parse(html, nil, 'utf-8')
end

def shape_url(href, url)
  return if href.nil?
  return href if href.start_with?("http")
  uri = URI.parse(url)
  return uri.scheme + ':' + href if href.start_with?("//")
  base_url = uri.scheme + "://" + uri.host
  href = '/' + href unless href.start_with?('/')
  base_url + href
end

#標準出力をバッファリングしない
STDOUT.sync = true
